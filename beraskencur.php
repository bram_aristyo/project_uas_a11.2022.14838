<img style="justify-content: center;margin-left:2px;padding-top:65px;" src="picture/beraskencur.png" class="img-fluid"
    alt="...">
<div style="background-color: white;padding-top: 25px;padding-bottom: 10px;text-align: justify;">
    <div class="container" style="padding-left: 150px;padding-right: 150px;">
        <div class="row">
            <div class="col-sm" style="text-align:left;">
                <div class="content1" style="text-transform: uppercase;">Beras KEncur </div>
                <div class="content2" style="text-transform: capitalize;">What is Jamu and How to Make Jamu Beras Kencur
                </div>
                <div class="content3">Beras Kencur is a javanese refreshing drink. This drink is also classified as
                    herbal medicine because it has the property of increasing appetite. Beras Kencur is very popular
                    because it has a sweet and fresh taste. </div>
                <div class="content4">BY <a
                        href="https://www.ajourneybespoke.com/journey/food/jamu-make-jamu-beras-kencur"
                        target="_blank">ajourneybespoke.com</a></div>
                <div class="content6" style="padding-top: 20px;
                        font-family: Cambria,
                        Cochin,
                        Georgia,
                        Times,
                        'Times New Roman',
                        serif;
                        font-weight: bold;
                        font-size: 20px;
                        ">
                    How To Make Beras Kencur </div>
                <div class="content5">Jamu is Indonesia’s system of traditional herbal medicine used by many for the
                    prevention and treatment of a variety of ailments. Many recipes for Jamu tonics utilise the flora
                    that is indigenous to tropical Indonesia including roots and rhizomes (eg: ginger, galangal,
                    turmeric),
                    flowers (eg: roselle, cloves and butterfly pea),
                    bark (cinnamon, secang),
                    leaves (eg: kaffir lime,
                    lemongrass),
                    and seeds & fruits (eg: peppercorns). </div>
                <div class="content7" style="padding-top:10px;">Ingredients </div>
                <div class="content8">
                    <ul style="padding-left: 15px;">
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            200g rice</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            50g kencur (kaempferia galangal) </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            30g ginger</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            130g palm sugar or coconut sugar</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            2Tb fresh lime juice</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            30g tamarind paste </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            2 pandan leaves (knotted together)</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            1.5 litres+of fresh water</li>
                    </ul>
                </div>
                <div class="content7">How to make </div>
                <div class="content8">
                    <ol style="padding-left: 15px;padding-top: 30px;">
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Return rice to bowl and cover with fresh water. Allow to soak for 3 hours before commencing
                            recipe.</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Peel,
                            wash and chop galangal,
                            ginger and tumeric. </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Into a blender add soaked rice (drained),
                            galangal,
                            ginger,
                            tumeric,
                            tamarind and palm sugar. </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Pour mixture into a saucepan and add 500ml fresh water as well as knotted pandan leaves,
                            lime juice and salt. </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Stir constantly until mixture reaches a very light simmer (approx. 5 minutes). This will
                            allow the palm sugar to dissolve and the essential oils in the pandan leaves to be
                            released.** Do not overheat as this will cook the rice. </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Strain mixture either through a muslin cloth or a fine sieve. </li>
                    </ol>
                </div>
                <div class="content5">Most commonly,
                    white rice is used in this recipe,
                    although I definitely prefer to use brown- or red rice to increase the nutrient content of the final
                    product. Using brown or red rice will yield a thicker Jamu so adjust your Jamu according to your
                    preference - add more fresh water if you prefer a thinner Jamu. Many recipes for Beras Kencur do not
                    include turmeric as an ingredient. I've added turmeric as it imparts a lovely earthy flavour and
                    rich colour (It is also believed to possess anti-inflammatory properties).

                    Don’t add all the sugar at once - I prefer my Jamu less sweet. Often traditional recipes call for
                    extra sugar so as a general rule,
                    always add half of the prescribed sugar then adjust from there. Shake bottle before consumption as
                    the heavier rice will fall to the bottom. </div>
            </div>
        </div>
    </div>
</div>