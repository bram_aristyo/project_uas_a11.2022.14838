<img style="justify-content: center;margin-left:2px;padding-top:65px;" src="picture/historyhead.png" class="img-fluid" alt="...">
        <div style="background-color: white;padding-top: 25px;padding-bottom: 10px;text-align: justify;">
            <div class="container" style="padding-left: 150px;padding-right: 150px;">
                <div class="row">
                    <div class="col-sm" style="text-align:left;">
                        <div class="content1">
                            THE HISTORY OF JAMU
                        </div>
                        <div class="content2">
                            Jamu is a complex mixture of herbs that is applied
                            as part of traditional healing and body care.
                        </div>
                        <div class="content3">
                            The
                            history of jamu according to experts in Javanese
                            Linguistic is that the term of “Jamu” originated from
                            Javanese language “Djampi or Jampi”
                        </div>
                        <div class="content4">
                            BY <a href="https://www.davidpublisher.com/index.php/Home/Article/index?id=30977.html"
                                target="_blank">Journal of Food Science and Engineering 7 (2017)</a>
                        </div>
                        <div class="content5">
                            Jamu is claimed to have
                            origenated in the Mataram Kingdom, some 1,300
                            years ago and heavily influenced by Ayurveda from
                            India [4]. The jamu culture also reached a pinnacle
                            during the era of Majapahit Kingdom in East Java, as
                            a now to be come famous beauty treatments in the
                            Kraton (Palace).
                        </div>
                        <div class="content5">
                            Jamu is an Indonesian terminology or language
                            (bahasa Indonesia) for Indonesian origin of herbal
                            preparation. The word jamu carries the meaning of
                            “guest, hospitality, made from plants”, and is the
                            traditional wisdom carried through generations of
                            Indonesian families. Produced with natural ingredients
                            including herb, the knowledge of this traditional soapThe utilisation of herb for medication
                            and beauty
                            care can be found in many cultural and art products
                            such as temple’s relief, inscrition stones, neolithic
                            stones, paintings, manuscripts, etc. During the Hindu
                            and Budhis Era at the First Century up to 16th
                            Century our ancestors had the tradition of making and
                            drinking jamu.
                        </div>
                        <img style="justify-content: center;width: 100%;" src="picture/history.png" class="img-fluid"
                            alt="...">
                        <div class="content5">
                            The knowledge and formulation of jamu had been
                            found in the “Serat Kawruh” manuscript of jamu in
                            Surakarta and Yogyakarta Palaces that was written in
                            year of 1831. It contained 1,166 prescriptions, 922 of
                            which were jamu preparations. In the manuscript
                            “Serat Centhini” was believed to contain the most
                            comprehensive references of medical treatment in
                            ancient Java [1].
                            health food, functional food products and spaor beauty
                            products. Functions and benefits of jamu products are
                            also growing well, and not only as a traditional
                            medicine but it has been processed into a variety of
                            products such as standardized jamu, phytoph
                            armaceutical jamu, products for health maintenance,
                            diseases prevention, restoration of health, fitness,
                            relaxation, beauty treatments and jamu for animal or
                            livestock care.
                            At present one could buy easily readymade jamu
                            packed modernly in the form of powder, pills,
                            capsules, drinking liquid and ointments. Of course
                            there are still jamu shops, which sell only ingredients
                            or prepare the jamu on spot as required by buyers.
                            That some women are roaming the street to sell jamu,
                            is a common view across the country. There are many
                            large companies such as Air Mancur, Nyonya Menir
                            or Djamu Djago, Sido Muncul, Marta Tilaar etc.
                            which is provided by modern machineries for
                            processing.
                        </div>
                    </div>
                </div>
            </div>
        </div>
