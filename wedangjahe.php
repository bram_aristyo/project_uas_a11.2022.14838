<img style="justify-content: center;margin-left:2px;padding-top:65px;" src="picture/jahe.png" class="img-fluid"
    alt="...">
<div style="background-color: white;padding-top: 25px;padding-bottom: 10px;text-align: justify;">
    <div class="container" style="padding-left: 150px;padding-right: 150px;">
        <div class="row">
            <div class="col-sm" style="text-align:left;">
                <div class="content1" style="text-transform: uppercase;">wedang jahe</div>
                <div class="content2" style="text-transform: capitalize;">How to Make Authentic Indonesian Ginger Tea -
                    Wedang Jahe
                </div>
                <div class="content3">Originating in Central and East Java, Wedang Jahe is a popular drink which can be
                    served either hot or cold.

                    Traditionally made from a simple combination of fresh ginger and rock sugar (or palm sugar in this
                    case) along with hot water, Wedang Jahe is most frequently consumed in the Wet Season when it is
                    believed that the cooler weather brings about the onset of flu-like symptoms (‘masuk angin’).

                    It is indeed a comforting drink - one that I enjoy sipping hot, but equally enjoy drinking cold.
                    With the addition of a handful of ice cubes, some mint leaves and a stick of lemongrass as a swizzle
                    stick, you have the most refreshing health tonic to enjoy anytime!</div>
                <div class="content4">BY <a
                        href="https://www.ajourneybespoke.com/journey/food/make-authentic-indonesian-ginger-tea-wedang-jahe"
                        target="_blank">ajourneybespoke.com</a></div>
                <div class="content6" style="padding-top: 20px;
                        font-family: Cambria,
                        Cochin,
                        Georgia,
                        Times,
                        'Times New Roman',
                        serif;
                        font-weight: bold;
                        font-size: 20px;
                        ">
                    How To Make Wedang Jahe</div>
                <div class="content5">rab your ingredients and head to the kitchen. In no time your home will be filled
                    with the beautiful aroma of traditional Indonesian spices and you will have the satisfaction of
                    creating one of the most delicious Jamu recipes to share with family and friends. </div>
                <div class="content7" style="padding-top:10px;">Ingredients </div>
                <div class="content8">
                    <ul style="padding-left: 15px;">
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            150 grams fresh ginger peeled, bruised and sliced</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            280 grams of palm or coconut palm sugar - roughly chopped</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            3 lengths of lemongrass, bruised and chopped</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            3 pandan leaves (tied)</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            1 Tb black pepper corns</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            6 cardamom pods (cracked to release flavour)</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            12 cloves</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            1.5 Litres of fresh water</li>
                    </ul>
                </div>
                <div class="content7">How to make </div>
                <div class="content8">
                    <ol style="padding-left: 15px;padding-top: 30px;">
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            In a non-stick pan, dry fry the ginger slices until fragrant.</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Transfer ginger into a large saucepan and add all other ingredients.</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Bring to boil then cover pot and reduce heat, continuing to simmer for a minimum of 5
                            minutes, allowing sugar to dissolve. This will also allow the flavours to infuse.</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Remove from heat and allow Wedang Jahe to steep for a further 5 — 10 minutes.</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Strain and either serve immediately or allow to cool then transfer to sterilised bottles.
                        </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Keep in fridge for up to 5 days.</li>
                    </ol>
                </div>
                <div class="content5">After straining, return all solid ingredients to saucepan and add 3 quills of
                    cinnamon. Add a further 1 litre of water and a couple of spoons of palm sugar or coconut nectar
                    sugar. Return to the boil and repeat steps 3 - 5 as above.</div>
            </div>
        </div>
    </div>
</div>