<img style="justify-content: center;margin-left:2px;padding-top:65px;" src="picture/temulawak.png" class="img-fluid"
    alt="...">
<div style="background-color: white;padding-top: 25px;padding-bottom: 10px;text-align: justify;">
    <div class="container" style="padding-left: 150px;padding-right: 150px;">
        <div class="row">
            <div class="col-sm" style="text-align:left;">
                <div class="content1" style="text-transform: uppercase;">
                    Temulawak
                </div>
                <div class="content2" style="text-transform: capitalize;">
                    How To Make Temulawak Tea, Indonesian Traditional Medicine Recipe For Stamina
                </div>
                <div class="content3">
                    Temulawak is one of the plants native to Indonesia that is similar in shape to turmeric.
                    Having the Latin name Curcuma xanthorrhiza, this herbal plant has been trusted by the people
                    of Indonesia for its benefits. Therefore, temulawak is often cultivated in the highlands to
                    be processed into supplements or sold directly.
                </div>
                <div class="content4">
                    BY <a href="https://tuankelapa.com/how-to-make-temulawak-tea-indonesian-traditional-medicine-recipe-for-stamina/"
                        target="_blank">tuankelapa.com</a>
                </div>
                <div class="content6" style="padding-top: 20px;
  font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
  font-weight: bold;
  font-size: 20px;">
                    How To Make Temulawak Tea
                </div>
                <div class="content5">
                    There are various versions of temulawak herbal medicine, this version is suitable for those
                    of you who want to increase stamina with not too many ingredients. With just the addition of
                    cinnamon and Tuan Kelapa’s palm sugar, your temulawak jamu will be ready in just a few
                    minutes.
                </div>
                <div class="content7" style="padding-top:10px;">
                    Ingredients
                </div>
                <div class="content8">
                    <ul style="padding-left: 15px;">
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            1-2 teaspoons of temulawak powder or fresh sliced ​​temulawak</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            800 ml of water</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Pinch of cinnamon powder</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            2 teaspoons of Tuan Kelapa’s palm sugar</li>

                    </ul>
                </div>
                <div class="content7">
                    How to make
                </div>
                <div class="content8">
                    <ol style="padding-left: 15px;padding-top: 30px;">
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Bring the water into a boil</li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Add two tablespoons of temulawak, stir briefly and bring to a boil over low heat.
                        </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Turn off the heat, add a pinch of cinnamon powder, stir again.
                        </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Strain the temulawak tea into a drinking bowl, leave it at room temperature for up
                            to five minutes.
                        </li>
                        <li
                            style="font-size: 15px;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            Ready to be served with Tuan Kelapa’s palm sugar for a more delicious taste
                        </li>


                    </ol>
                </div>
                <div class="content5">
                    Indonesia has many traditional stamina-boosting drinks which are familiarly known as jamu.
                    Various herbs in Indonesia are made from a mixture of spices that are boiled and served
                    warm.

                    One of the most commonly consumed and well-known herbs is Temulawak. Temulawak, Curcuma
                    zanthorrhiza or also known as Javanese ginger or Javanese turmeric is an herbal plant that
                    at first glance looks similar to turmeric but has a paler yellow colour than turmeric.

                    The main content of temulawak rhizome is protein, carbohydrates and essential oils
                    consisting of camphor, glucoside, turmerol and curcumin. The content of curcumin is good for
                    anti-inflammatory and anti-hepototic.
                </div>
            </div>
        </div>
    </div>
</div>