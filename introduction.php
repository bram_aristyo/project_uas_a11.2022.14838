
<img style="justify-content: center;padding-top:65px;" src="picture/introductionhead.png" class="img-fluid" alt="...">
<div style="background-color: white;padding-top: 25px;padding-bottom: 10px;text-align: justify;">
    <div class="container" style="padding-left: 150px;padding-right: 150px;">
        <div class="row">
            <div class="col-sm" style="text-align:left;">
                <div class="content1">
                    INTRODUCTION
                </div>
                <div class="content2">
                    Jamu is traditional herbal medicine and a healthy drink considered by some Indonesians to be
                    a
                    primary element in their lives.
                </div>
                <div class="content3">
                    The term of jamu may be derived from ancient Javanese language “Djampi” that means a healing
                    method using herb during the era of Ancient Java emerged since the 9th century AD.
                </div>
                <div class="content4">
                    BY <a href="https://www.davidpublisher.com/index.php/Home/Article/index?id=30977.html"
                        target="_blank">Journal of Food Science and Engineering 7 (2017)</a>
                </div>
                <div class="content5">
                    Jamu is consume either in fresh or in processed one. Some jamu sellers still
                    found carrying their wares, in a basket fastened to their torsos using a sling made of
                    fabric,
                    and visit door to door in local
                    neighborhoods. The name is Jamu gendong. The use of the jamu remains the same as the
                    ancestors
                    did. The visible proof is the use of
                    traditional herbal medicine of various type of “medical plants”, either from the leaves, the
                    fruits, the roots, the flowers or the barks,
                    etc. It is truly back to nature. These herbal medicine had been used since the ancient time
                    up
                    to now, it is largely consumed by people
                    of different levels: lower, middle and upper, in the villages and in the big cities. Jamu as
                    a
                    cultural heritage has a big potential asset
                    of Indonesia and is also one of Indonsia’s cultural products based on local resources and
                    the
                    creativity of the nation.
                </div>
                <img style="justify-content: center;width: 100%;" src="picture/intro.png" class="img-fluid"
                    alt="...">
                <div class="content5">
                    Indonesia has
                    natural resourses of plants totaling about 30,000 species of flowering plants, which is the
                    third largest in the world after Brazil and
                    Zaire, including 7,000 species of medicinal plants, 940 species one could buy easily
                    readymade
                    jamu packed modernly in the form of
                    powder, pills, capsules, drinking liquid and ointments. Of course there are still jamu
                    shops,
                    which only sell for those have been
                    identified, 283 species are listed as raw materials that are used routinely in the medicine
                    industry. Jamu from time to time has been
                    widely accepted in almost every country in the world. It is not only as a preventive
                    maintenance, but also as a curative treatment for
                    acute and chronic treatment. At present ingredients of jamu and its preparation spot as
                    required
                    by buyers. Some women are roaming
                    the street to sell jamu, which is a common view across the country. At present time, jamu
                    has
                    also been produced in mass by
                    manufacturers for export, and mostly concerns on quality, consistency, and cleanliness
                    either
                    locally or internationally distributed.
                </div>
            </div>
        </div>
    </div>
</div>
