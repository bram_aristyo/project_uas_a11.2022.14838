<img style="justify-content: center;margin-left:2px;padding-top:65px;" src="picture/culturehead.png" class="img-fluid" alt="...">
<div style="background-color: white;padding-top: 25px;padding-bottom: 10px;text-align: justify;">
    <div class="container" style="padding-left: 150px;padding-right: 150px;">
        <div class="row">
            <div class="col-sm" style="text-align:left;">
                <div class="content1" style="text-transform: uppercase;">
                    Ingredient and the Culture of Jamu
                </div>
                <div class="content2">
                    General jamu ingridients more or less depend on
                    the region, in different regions have been created from
                    a sence of the work that has a great kulinasi derived
                </div>
                <div class="content3">
                    It is called Jamu Nusantara. Here are some
                    of examples of selected jamu products according to
                    region and ethnicity in Indonesia as follows:
                </div>
                <div class="content4">
                    BY <a href="https://www.davidpublisher.com/index.php/Home/Article/index?id=30977.html"
                        target="_blank">Journal of Food Science and Engineering 7 (2017)</a>
                </div>
                <div class="content5">
                    (1) Jamu ingridients of Badui Banten, it is jamu
                    ingredients derived from the Badui tribe in rural of
                    Banten Province are used for treatment of broken
                    bones, sprains, arthritis or post accident recovery.
                    People believed this jamu to be able to cure diseases,
                    restore fitness and fertility.
                    (2) Jamu Bali in everyday life apply on spa menus,
                    advertisements and the local apotek. In Bali, herbs has
                    been used and implemented for healing rather than
                    beauty. Jamu traditional spa was the first use in
                    traditional Indonesian treatments in Bali since 1988
                    serving international travelers. They started a trend for
                    jamu-based products, which makes Bali into the
                    limelight of the world of spas.
                </div>
                <div class="content5">
                    (3) General Jamu ingidients of Javanese people, it
                    figurizes some types of jamu widely consumed by the
                    people of the island of Java. Some of them are (a)
                    jamu beras kencur which is believed to be able to cure
                    body that has no appetite and is also able to stimulate
                    appetite, and then increase the appetite body to make
                    healthier; (b) Jamu kunir asem is consumed to nourish
                    the body.
                    (4) There are various kinds of jamu to combat
                    different kinds of illness. In principle there are two
                    types of jamu (a) the first jamu is to maintain physical
                    fitness and health, the locally popular are Galian
                    Singset (to keep women body fit and slim) and Sehat
                    Lelaki (to keep men body healthy); (b) The second is
                    jamu to cure various kinds of illness. Except the above,
                    there are special jamu created with the purpose to
                    maintain a loving family harmony. The popular
                    products among other are Sari Rapet, which makes a
                    women sexual organ in a good condition, as for the
                    man the matched product is jamu Kuat Lekaki (strong
                    man). The Javanese are also taking great care to
                    pregnant women during pre and postnatal period by
                    producing the related jamu. There are also jamu for
                    the babies.
                </div>
                <img style="justify-content: center;width: 100%;" src="picture/culture.png" class="img-fluid" alt="...">
                <div class="content5">
                    The herbs for Jamu come from spices, leaves. fruits,
                    barks and flowers. There are hundreds of herbs for
                    jamu prescriptions, among other are:
                    Spices: Ginger (Zingiber Officinale), Lempuyang
                    (Zingiber Oronaticum), Temu Lawak/ Wild Ginger
                    (Curcuma Cautkeridza), Kunyit/ Tumeric (Curcuma
                    Domestica), Kencur/ Greater Galingale (Kaemferi
                    Galanga), Lengkuas/ Ginger Plant (Elpina Galanga),
                    Bengle (Zingiber Bevifalium);
                    Leaves: Secang (Caesalpinia Sappan Hinn),
                    Sambang Dara (Rexco Ecaria Bicolar Hassk),
                    Brotowali (Tiospora Rumpii Boerl), dan Adas
                    (Foeniculum Vulgare Mill);
                    Fruits: Jeruk Nipis/ Calamondin (Citrae
                    Aurantifalia Sivingle), Ceplukan (Physalic Angulata
                    Him), Nyamplung (Calophylum Inaphyllu);
                    Barks: Kayu Manis/ Cinamon (Gijeyzahyza Glabra);
                    Flowers: Melati/ Yasmin (Jataninum Sunbac Ait),
                    Rumput Alang-alang (Gramineae).
                    It is worth to note that some jamu factories in Java
                    are exporting its products. Besides the export of
                    readymade jamu, 25 kinds of herbal plants and
                    ingredients are also in the list of export to Europe,
                    Australia, USA, Japan, etc. The people like to
                    consume jamu due to: availability in many places,
                    comparatively cheap price, no side effects.
                </div>
            </div>
        </div>
    </div>
</div>