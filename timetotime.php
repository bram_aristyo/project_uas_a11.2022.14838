<img style="justify-content: center;margin-left:2px;padding-top:65px;height:500px;width:1650px;" src="picture/timetotimehead.png" class="img-fluid" alt="...">
<div style="background-color: white;padding-top: 25px;padding-bottom: 10px;text-align: justify;">
    <div class="container" style="padding-left: 150px;padding-right: 150px;">
        <div class="row">
            <div class="col-sm" style="text-align:left;">
                <div class="content1" style="text-transform: uppercase;">
                    Jamu from Time to Time
                </div>
                <div class="content2">
                    For the people of Indonesia, jamu is hereditary
                    ingredient needed to be maintained and developed.
                </div>
                <div class="content3">
                    The recipes of jamu collected from the palace and the
                    knowledge of jamu ingredients have grown in the
                    society today.
                </div>
                <div class="content4">
                    BY <a href="https://www.davidpublisher.com/index.php/Home/Article/index?id=30977.html"
                        target="_blank">Journal of Food Science and Engineering 7 (2017)</a>
                </div>
                <div class="content5">
                    For the people of Indonesia, jamu is hereditary
                    ingredient needed to be maintained and developed.
                    The recipes of jamu collected from the palace and the
                    knowledge of jamu ingredients have grown in the
                    society today. At the present time, the traditional jamu
                    produced from herbs and other natural ingredients can
                    be found in women who sell jamu by carrying etheir
                    product on their back (“gendong”), so this
                    traditionally made jamu is called “Jamu Gendong”.
                    The method of using the jamu remains the same as the
                    ancestors did. Some are consumed by drinking it and
                    some are for outside application. However, along with
                    advancement of technology and an efficiency of time
                    the fresh jamu and sometimes processed jamu or
                    packed jamu has been sold by women using bike or
                    motor bike on a large wooden box attached to her
                    motorbike, stocked with several large bottles and a
                    small pharmacy sachet of various cures [5].
                </div>
                <div class="content5">
                    Forms of jamu products varied from fresh jamu,
                    boiling jamu, powders, pills, capsules, even later jamu
                    is developed into different types of beverages and
                    health food, functional food products and spaor beauty
                    products. Functions and benefits of jamu products are
                    also growing well, and not only as a traditional
                    medicine but it has been processed into a variety of
                    products such as standardized jamu, phytoph
                    armaceutical jamu, products for health maintenance,
                    diseases prevention, restoration of health, fitness,
                    relaxation, beauty treatments and jamu for animal or
                    livestock care.
                    At present one could buy easily readymade jamu
                    packed modernly in the form of powder, pills,
                    capsules, drinking liquid and ointments. Of course
                    there are still jamu shops, which sell only ingredients
                    or prepare the jamu on spot as required by buyers.
                    That some women are roaming the street to sell jamu,
                    is a common view across the country. There are many
                    large companies such as Air Mancur, Nyonya Menir
                    or Djamu Djago, Sido Muncul, Marta Tilaar etc.
                    which is provided by modern machineries for
                    processing.
                </div>
                <div style = "padding-top: 20px;padding-bottom: 20px">
                <img style="justify-content: center;width: 100%;padding-bottom:30px;padding-top: 20px;" src="picture/timetotime1.png" class="img-fluid" alt="...">
                <img style="justify-content: center;width: 100%;" src="picture/timetotime2.png" class="img-fluid" alt="...">
</div>
                
            </div>
        </div>
    </div>
</div>