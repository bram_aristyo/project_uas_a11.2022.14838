<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script type="text/javascript">
        function zoom() {
            document.body.style.zoom = "100%"
        }
    </script>
    <script>
        $(document).ready(function () {
            $("#div2").fadeIn(1000);
        });
        $(document).ready(function () {
            $("#loadingpage").load('home.php');
            $("#introduction").click(function () {
                $("#div2").fadeOut(10);
                $("#loadingpage").load('introduction.php');
                $("#div2").fadeIn(1000);
            });
            $("#home").click(function () {
                $("#div2").fadeOut(10);
                $("#loadingpage").load('home.php');
                $("#div2").fadeIn(1000);
            });
            $("#History").click(function () {
                $("#div2").fadeOut(10);
                $("#loadingpage").load('history.php');
                $("#div2").fadeIn(1000);
            });
            $("#timetotime").click(function () {
                $("#div2").fadeOut(10);
                $("#loadingpage").load('timetotime.php');
                $("#div2").fadeIn(1000);
            });
            $("#culture").click(function () {
                $("#div2").fadeOut(10);
                $("#loadingpage").load('culture.php');
                $("#div2").fadeIn(1000);
            });
            $("#beraskencur").click(function () {
                $("#div2").fadeOut(10);
                $("#loadingpage").load('beraskencur.php');
                $("#div2").fadeIn(1000);
            });
            $("#wedangjahe").click(function () {
                $("#div2").fadeOut(10);
                $("#loadingpage").load('wedangjahe.php');
                $("#div2").fadeIn(1000);
            });
            $("#temulawak").click(function () {
                $("#div2").fadeOut(10);
                $("#loadingpage").load('temulawak.php');
                $("#div2").fadeIn(1000);
            });
        });
    </script>
    <!-- pict 500 x 1650 -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="styleuas.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto Mono' rel='stylesheet'>
    <title>BRAMBRIMBRUM</title>
</head>

<body style="background-color: black;" onload="zoom()">

    <!-- navbar -->
    <div class="container">
        <div class="navbar fixed-top">
            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"">menu  </a>
          <div class=" dropdown-menu" style="background-color: black;padding-right: 275px;">
                    <a class="dropdown-item" id="introduction" href="#">Introduction</a>
                    <a class="dropdown-item" id="History" href="#">The History of Jamu</a>
                    <a class="dropdown-item" id="timetotime" href="#">Jamu from Time to Time</a>
                    <a class="dropdown-item" id="culture" style="padding-bottom:25px;" href="#">Culture of Jamu</a>
                    <a class="dropdown-item" style="padding-bottom:0px;font-size:12px;" id="beraskencur" href="#">Beras
                        Kencur</a>
                    <a class="dropdown-item" style="padding-bottom:0px;font-size:12px;" id="temulawak" 
                        href="#">Temulawak</a>
                    <a class="dropdown-item" id="wedangjahe" style="padding-bottom:25px;font-size:12px;" 
                        href="#">Wedang
                        Jahe</a>
                    <a class="dropdown-item" id="home" href="#" style="text-decoration: underline;">Back to Home</a>
            </div>
        </div>
        <a class="mid-navbar">nusantara herbs</a>
        <a target="_blank" href="UTS/nasi_goreng_page.html" class="right-navbar">+throwback</a>
    </div>
    </div>
    <!-- navbar end -->
    <div id="div2" style="display: none;">
        <!-- content -->
        <div id="loadingpage"></div>
        <!-- content end -->

        <!-- footer -->
        <div class="foooter">
            <div class="disstance2"></div>
            <a id="home" href="index.php">BRAMBRIMBRUM.COM</a>
            <ul class="socialmedia">
                <li><a href="https://www.instagram.com/thisisbillgates/" target="_blank">instagram</a></li>
                <li><a href="https://www.youtube.com/@mitocw" target="_blank">youtube</a></li>
                <li><a href="https://twitter.com/elonmusk" target="_blank">twitter</a></li>
            </ul>
            <p>"Learn from yesterday, live for today, hope for tomorrow." <br>- albert enstein -</p>
        </div>
    </div>
    <!-- footer end -->
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>